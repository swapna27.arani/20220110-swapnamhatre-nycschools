# 20220110-SwapnaMhatre-NYCSchools


# NYC School: SAT Score


# Description :

The project implements the below functionality

    ● Query NYC Schools

    ● Display the list of Schools

    ● Query SAT Score for given school
    
    ● Display the SAT Score for selected school


# Android Design Inspiration

    ● MVVM

        ○ Separation Of Concerns

        ○ Three Layers
            ■ UI

            ■ Domain 

            ■ Data

    
    ● Jet Pack Compose : For Declarative UI Design

    ● Navigation Host : For Navigating within APP and maintaining backstack entry

    ● Repository : Abstract the Database and Network queries

    ● Flow : Kotlin Flow

    ● Room Database : For efficient and convenient handling

    ● Paging Library

    ● Hilt : Dependency Injection

    ● Retrofit : For Network calls
    

 